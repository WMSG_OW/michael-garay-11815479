#include "Pokemon.h"
#include "Player.h"
#include<iostream>
#include <time.h>

using namespace std;

Player* Trainer = new Player("");

Pokemons::Pokemons()
{
	this->baseHP = 0;
	this->hp = 0;
	this->baseDamage = 0;
	this->xp = 0;
	this->maxXp = 0;
	this->level = 0;
}

Pokemons::Pokemons(int baseHP, int hp, int baseDamage, int xp, int maxXp, int level)
{
	this->baseHP = baseHP;
	this->hp = hp;
	this->baseDamage = baseDamage;
	this->xp = xp;
	this->maxXp = maxXp;
	this->level = level;
}

void Pokemons::stats(int baseHP, int hp, int baseDamage, int xpDrop, int maxXp, int level, int inventorySize, int starterLevel)
{

	for (int i = 0; i < level; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		baseDamage = baseDamage + (baseDamage * 0.10);
		xpDrop = xpDrop + (xpDrop * 0.20);
		maxXp = maxXp + (maxXp * 0.20);
	}
	for (int i = 0; i < starterLevel; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		baseDamage = baseDamage + (baseDamage * 0.10);
		xpDrop = xpDrop + (xpDrop * 0.20);
		maxXp = maxXp + (maxXp * 0.20);
	}
	cout << "Level :  " << starterLevel << endl;
	cout << pokemonInventory[0] << endl;
	cout << "HP : "  << baseHP << endl;
	cout << " Base Damage : " << baseDamage << endl;
	cout << "Experience points :" << xp << "/" << maxXp << endl;
	for (int i = 1; i < pokemonInventorySize; i++)
	{
		cout << "Level : "  << level << endl;
		cout << pokemonInventory[i] << endl;
		cout << "HP : " << baseHP << endl;
		cout << "Base Damage:"  << baseDamage << endl;
		cout << "Experience points :" << xp << "/" << maxXp << endl;
	}
}

void Pokemons::battle( int baseHP, int baseDamage, int encounter, int xp, int maxXp, int pokemonInventorySize)
{
	for (int i = 0; i < level; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		baseDamage = baseDamage + (baseDamage * 0.10);
		xpDrop = xpDrop + (xpDrop * 0.20);
		maxXp = maxXp + (maxXp * 0.20);
	}
	for (int i = 0; i < starterLevel; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		baseDamage = baseDamage + (baseDamage * 0.10);
		maxXp = maxXp + (maxXp * 0.20);
	}
	Trainer->pokemonInventory[6];
	for (int i = 0; i < pokemonInventorySize; i++)
	{
		do
		{
			cout << pokemonInventory[pokemonInventorySize] << "dealt " << baseDamage << " damage  " << endl;
			cout << wildPokemon[encounter] << "dealt " << baseDamage << " damage  " << endl;
			baseHP = baseHP - baseDamage;
			baseHP = baseHP - baseDamage;
			cout << "Level : " << level << endl;
			cout << pokemonInventory[i] << endl;
			cout << "HP : " << baseHP << endl;
			cout << "Base Damage: " << baseDamage << endl;
			cout << "Experience points : " << xp << "/" << maxXp << endl;

			cout << "Level : " << level << endl;
			cout << wildPokemon[encounter] << endl;
			cout << "HP : " << baseHP << endl;
			cout << "Base Damage:" << baseDamage << endl;
			cout << "Experience points :" << xp << "/" << maxXp << endl;
			system("pause");
			system("cls");
			if (baseHP <= 0)
			{
				pokemonInventorySize++;
			}
		} while (baseHP > 0);
		if (baseHP <= 0 && pokemonInventorySize == 7)
		{
			cout << "Player has fainted " << endl;
		}
	}
}

void Pokemons::encounterPokemons(int encounter, int level, int baseHP, int baseDamage, int xp, int maxXp)
{
	for (int i = 0; i < level; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		baseDamage = baseDamage + (baseDamage * 0.10);
		xpDrop = xpDrop + (xpDrop * 0.20);
		maxXp = maxXp + (maxXp * 0.20);
	}
	cout << "You have encountered a wild : " << wildPokemon[encounter] << endl;
	cout << "Level : " << level << endl;
	cout << "HP : " << baseHP << endl;
	cout << "Base Damage:" << baseDamage << endl;
	cout << "Experience points :" << xp << "/"  << maxXp << endl;

	cout << "What would you like to do? " << endl;
	cout << "[5] Battle     [6] Capture      [7] Run" << endl;
}

void Pokemons::capturePokemon(int capture, int pokemonInventorySize, int encounter)
{
	Trainer->pokemonInventory;

	if (capture > 3)
	{
		cout << " got out and ran away" << endl;
	}
	else if (capture <= 3)
	{
		for (int i = 1; i < pokemonInventorySize; i++)
		{
			pokemonInventorySize++;
			pokemonInventory[i] = wildPokemon[encounter];
		}
	}
}

void Pokemons::starter(int starterChoice)
{
	if (starterChoice == 0)
	{
		pokemonInventory[0] = "Bulbasaur";
	}
	else if (starterChoice == 1)
	{
		pokemonInventory[0] = "Charmander";
	}
	else if (starterChoice == 2)
	{
		pokemonInventory[0] = "Squirtle";
	}

	cout << "Your journey begins now" << endl;
	system("pause");
	system("cls");
}




